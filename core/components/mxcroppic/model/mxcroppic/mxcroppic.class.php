<?php


class mxCroppic {

    public  $modx;
    public  $config = array();

    function __construct( modX &$modx, array $config = array() ) {
        $this->modx   =& $modx;
        
        $cPath = $this->modx->getOption( 'cPath', $config , $this->modx->getOption( 'core_path' ) . 'components/mxcroppic/' );

        $this->_config = array_merge( array(
                'cPath'    => $cPath,
                'aPath'    => '//'.$modx->getOption('http_host') . MODX_ASSETS_URL . 'components/mxcroppic/',
                'debug'    => false,
                'chPath'   => $cPath . 'elements/chunks/',
                'chSuffix' => '.chunk.tpl',
                'snPath'   => $cPath . 'elements/snippets/',
                'snSuffix' => '.snippet.php'
        ), $config );

        if ( $this->_config['debug'] ) {
            $this->modx->setLogLevel( $this->_config['debug_lvl'] );
            $this->logError( 'pmSites::In Debug Mode' );
        }
    }

        /**
     * Process and return the output from a PHP snippet by name.
     *
     * @param string  $name   The name of the snippet.
     * @param array   $params An associative array of properties to pass to the
     * snippet.
     * @return string The processed output of the snippet.
     */
    public function getSnippet( $name, array $params= array (), $suffix = null ) { 
        $output= '';

        if ( !$this->_config['debug'] ) {
            $snippet = $this->modx->getObject( 'modSnippet', array( 'name' => $name ) );
        }

        if ( empty( $snippet ) ) {
            $suffix  = !is_null($suffix) ? $suffix : $this->_config['snSuffix'];
            $snippet = $this->_getSnippetSource( $name, $suffix );
            if ( $snippet == $name ) return $name;
        }

        if ( $snippet instanceof modSnippet ) {
            $snippet->setCacheable( false );
            $output= $snippet->process( $params );
        }

        return $output;
    }

    /**
     * Returns a modSnippet object from a template file.
     *
     * @access private
     * @param string  $name   The name of the Snippet. Will parse to name.snippet.php
     * @param string  $suffix The suffix to postfix the chunk with
     * @return modChunk/boolean Returns the modChunk object if found, otherwise
     * false.
     */
    protected function _getSnippetSource( $name, $suffix) {
        $snippet = $name;
        $f       = $this->_config['snPath'] . strtolower( $name ) . $suffix;

        if ( file_exists( $f ) ) {
            $o = file_get_contents( $f );
            //@var modSnippet $snippet
            $snippet = $this->modx->newObject( 'modSnippet' );
            $snippet->set( 'name', $name );
            $snippet->setContent( $o );
            
        }

        return $snippet;
    }

    public function parseChunk( $name, $properties = array(), $suffix = null ) {
        return $this->getChunk( $name, $properties, $suffix );
    }

    /**
     * Gets a Chunk and caches it; also falls back to file-based templates
     * for easier debugging.
     *
     * Will always use the file-based chunk if $debug is set to true.
     *
     * @access public
     * @param string  $name       The name of the Chunk
     * @param array   $properties The properties for the Chunk
     * @return string The processed content of the Chunk
     */
    public function getChunk( $name, $properties = array(), $suffix = null ) {
        $chunk = null;
        if ( !isset( $this->chunks[$name] ) ) {
            if ( !$this->_config['debug'] ) {
                $chunk = $this->modx->getObject( 'modChunk', array( 'name' => $name ) );
            }
            if ( empty( $chunk ) ) {
                $suffix  = !is_null($suffix) ? $suffix : $this->_config['chSuffix'];
                $chunk  = $this->_getTplChunk( $name, $suffix );
                if ( $chunk == $name ) return $name;
            }
            $this->chunks[$name] = $chunk->getContent();
        } else {
            $o = $this->chunks[$name];
            $chunk = $this->modx->newObject( 'modChunk' );
            $chunk->setContent( $o );
        }

        $chunk->setCacheable( false );
        return $chunk->process( $properties );
    }

    /**
     * Returns a modChunk object from a template file.
     *
     * @access private
     * @param string  $name   The name of the Chunk. Will parse to name.chunk.tpl
     * @param string  $suffix The suffix to postfix the chunk with
     * @return modChunk/boolean Returns the modChunk object if found, otherwise
     * false.
     */
    protected function _getTplChunk( $name, $suffix ) {
        $chunk = $name;
        $f     = $this->_config['chPath'] . strtolower( $name ) . $suffix;

        if ( file_exists( $f ) ) {
            $o = file_get_contents( $f );
            /** @var modChunk $chunk */
            $chunk = $this->modx->newObject( 'modChunk' );
            $chunk->set( 'name', $name );
            $chunk->setContent( $o );
        }

        return $chunk;
    }

    private function logDebug( $msg ) {
        if ( $this->modx->getLogLevel() <= modX::LOG_LEVEL_DEBUG ) {
            $this->modx->log( modX::LOG_LEVEL_DEBUG, $msg );
        }
    }

    private function logError( $msg ) {
        if ( $this->modx->getLogLevel() <= modX::LOG_LEVEL_ERROR ) {
            $this->modx->log( modX::LOG_LEVEL_ERROR, $msg );
        }
    }
}



    


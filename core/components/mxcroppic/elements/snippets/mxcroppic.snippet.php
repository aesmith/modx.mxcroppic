<?php

$out = '';
$sp  =& $scriptProperties;

$cPath        = $modx->getOption( 'cPath', $config , $modx->getOption( 'core_path' ) . 'components/mxcroppic/' );
$tplWrapper   = $modx->getOption( 'tplWrapper', $sp, 'mxCroppic' );

$regCSS[]     = $modx->getOption( 'regCSS', $sp, MODX_ASSETS_URL . 'components/mxcroppic/css/croppic.css');
$regScript[]  = $modx->getOption( 'regScripts', $sp, MODX_ASSETS_URL . 'components/mxcroppic/js/croppic.min.js');

$regCSS[]     = $modx->getOption( 'usrCSS', $sp, '');
$regScript[]  = $modx->getOption( 'usrScripts', $sp, '');

$sp['id']        = $modx->getOption( 'id', $sp, '#croppic');
$sp['cssW']      = $modx->getOption( 'cssW', $sp, '400px');
$sp['cssH']      = $modx->getOption( 'cssH', $sp, '208px');
$sp['cssBgImg']  = $modx->getOption( 'cssBgImg', $sp, MODX_ASSETS_URL . 'components/mxcroppic/img/placeholder.png');
$sp['cssBgClr']  = $modx->getOption( 'cssBgClr', $sp, '#efefef');
$sp['cssBrdr']   = $modx->getOption( 'cssBrdr', $sp, '3px solid #ccc');

$mxcroppic   = $modx->getService( 'mxcroppic', 'mxCroppic', $cPath . 'model/mxcroppic/', $sp );
if ( !( $mxcroppic instanceof mxCroppic ) ) {
  echo 'Error: Unable to find service mxCroppic';die;
}

$out = $mxcroppic->getChunk($tplWrapper,$sp);

if($out != ''){
	if(!empty($regCSS)){
		if(is_string($regCSS)){
			if($regCSS == ''){ continue; }
			$modx->regClientCSS($regCSS);
		}
		else if(is_array($regCSS)){
			foreach($regCSS as $css){
				if($regCSS == ''){ continue; }
				$modx->regClientCSS($css);
			}
		}
	}

	if(!empty($regScript)){
		if(is_string($regScript)){
			if($regClientScript == ''){ continue; }
			$modx->regClientScript($regScript);
		}
		else if(is_array($regScript)){
			foreach($regScript as $css){
				if($regClientScript == ''){ continue; }
				$modx->regClientScript($css);
			}
		}
	}

	$modCSS = $mxcroppic->getChunk('mxCroppic.modCSS',$sp);

    if($modCSS != ''){
		$modx->regClientStartupHTMLBlock($modCSS);
	}

}

return $out;
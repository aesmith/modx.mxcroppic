<style>
#[[+id]] {

  width: [[+cssW]];
  height: [[+cssH]];
  position: relative;
  border: [[+cssBrdr]];
  box-sizing: content-box;
  -moz-box-sizing: content-box;
  border-radius: 2px;
  background-image: url([[+cssBgImg]]);
  background-repeat: no-repeat;
  background-position: center;
  background-color: [[+cssBgClr]];

}
</style>